export default {
    props: {
        chartId: {
            type: String,
            default: 'myChart'
        },
        showXAxisLine: {
            type: Boolean,
            default: false
        },
        showYAxisLine: {
            type: Boolean,
            default: false
        }
    }
}