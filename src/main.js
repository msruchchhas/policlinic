import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './assets/css/feature.css'
import './assets/scss/style.scss'
import 'aos/dist/aos.css'
import 'bootstrap'

window.bootstrapJS = require('bootstrap')

createApp(App).use(router).mount('#app')
