import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Dashboard from '../views/dashboard/Index'
import Maintenance from '../views/maintenance/Index'

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard,
    meta: {
      title: 'Policlinic || Dashboard Template',
    }
  },
  {
    path: '/statistics',
    name: 'Statistics',
    component: Maintenance,
    meta: {
      title: 'Policlinic || Dashboard Template',
    }
  },
  {
    path: '/patients',
    name: 'Patients',
    component: Maintenance,
    meta: {
      title: 'Policlinic || Dashboard Template',
    }
  },
  {
    path: '/policlinic',
    name: 'Policlinic',
    component: Maintenance,
    meta: {
      title: 'Policlinic || Dashboard Template',
    }
  },
  {
    path: '/doctors',
    name: 'Doctors',
    component: Maintenance,
    meta: {
      title: 'Policlinic || Dashboard Template',
    }
  },
  {
    path: '/medicines',
    name: 'Medicines',
    component: Maintenance,
    meta: {
      title: 'Policlinic || Dashboard Template',
    }
  },
  {
    path: '/messages',
    name: 'Messages',
    component: Maintenance,
    meta: {
      title: 'Policlinic || Dashboard Template',
    }
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Maintenance,
    meta: {
      title: 'Policlinic || Dashboard Template',
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
  window.scrollTo(0, 0)
});

export default router
